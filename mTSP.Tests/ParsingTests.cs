using mTSP.Solver.Parsing;
using NUnit.Framework;

namespace mTSP.Tests
{
    public class ParsingTests
	{
		[TestCase("rat99")]
		[TestCase("pr1002")]
		public void ParseEmbdedResourceInput(string instance)
		{
			var map = new TspParser().ParseEmbdedResourceFile(instance);

			Assert.True(map != null);
		}

		[TestCase("berlin52", 52)]
		[TestCase("pr299", 299)]
		public void CheckParsedCitiesAmount(string instance, int expectedAmount)
		{
			var map = new TspParser().ParseEmbdedResourceFile(instance);

			Assert.AreEqual(map.CitiesAmount, expectedAmount);
		}

		[TestCase("eil51", 0, 37, 52)]
		[TestCase("pr152", 0, 2100, 1850)]
		public void CheckParsedCityCoordinates(string instance, int index, int expectedX, int expectedY)
		{
			var map = new TspParser().ParseEmbdedResourceFile(instance);

			Assert.True(map.Cities[index].X == expectedX && map.Cities[index].Y == expectedY);
		}
    }
}
