﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using System;
using mTSP.Solver.Models.Input;

namespace mTSP.Solver.Parsing
{
	public class TspParser
    {
		private const string CitiesCoordinatesSectionStart = "NODE_COORD_SECTION";
		private const string CitiesCoordinatesSectionEnd = "EOF";
		private const string InstancesDir = "mTSP.Solver.Instances";

		public Map ParseEmbdedResourceFile(string instanceName)
		{
			Stream resource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"{InstancesDir}.{instanceName}.tsp");
			StreamReader reader = new StreamReader(resource);
			
			var lines = new List<string>();
			string line = null;
			while ((line = reader.ReadLine()) != null)
				lines.Add(line);

			return ParseInputLines(lines);
		}

		private Map ParseInputLines(IEnumerable<string> lines)
		{
			try
			{
				lines = lines.SkipWhile(x => x != CitiesCoordinatesSectionStart).Skip(1);
				var cities = new List<City>();

				foreach (var l in lines)
				{
					if (l == CitiesCoordinatesSectionEnd)
						break;

					var numbers = l.Split(' ', StringSplitOptions.RemoveEmptyEntries);
					if (numbers.Length != 3)
						throw new ArgumentException($"Incorrect data line for city number {cities.Count + 1}");

					if (!Double.TryParse(numbers[1].Replace('.', ','), out double x))
						throw new ArgumentException($"Incorrect x coordinate for city number {cities.Count + 1}");
					if (!Double.TryParse(numbers[2].Replace('.', ','), out double y))
						throw new ArgumentException($"Incorrect y coordinate for city number {cities.Count + 1}");

					cities.Add(new City() { X = (int)x, Y = (int)y });
				}

				return new Map()
				{
					Cities = cities.ToArray(),
					CitiesAmount = cities.Count
				};
			}
			catch(ArgumentException) { throw; }
			catch(Exception) { throw new ArgumentException("Incorrect file format"); }
		}
    }
}
