﻿using CommandLine;
using mTSP.Solver.Parsing;
using System;

namespace mTSP.Solver
{
    class Program
    {
		static void Main(string[] args)
		{
			Parser.Default.ParseArguments<Options>(args)
				.WithParsed<Options>(opts => SolveProblem(opts))
				.WithNotParsed(errors =>
				{
					if (args.Length == 1 && args[0] == "--help")
						return;
					Printer.Error("Command line arguments parsing errors");
				});
		}

		private static void SolveProblem(Options o)
		{
			try
			{
				var map = new TspParser().ParseEmbdedResourceFile(o.Instance);
				Printer.Success($"Input file parsed. Cities amount: {map.CitiesAmount}");
			}
			catch(Exception e)
			{
				Printer.Error(e.Message);
			}
		}

		class Options
		{
			[Option('i', "instance", Required = true, HelpText = "The problem instance name which should be resolved")]
			public string Instance { get; set; }
		}
	}
}
