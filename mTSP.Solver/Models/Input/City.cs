﻿namespace mTSP.Solver.Models.Input
{
	public class City
    {
		public int X { get; set; }
		public int Y { get; set; }
    }
}
