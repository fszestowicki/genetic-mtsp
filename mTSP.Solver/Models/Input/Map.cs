﻿using System.Collections.Generic;

namespace mTSP.Solver.Models.Input
{
	public class Map
    {
		public int CitiesAmount { get; set; }
		public City[] Cities { get; set; }
    }
}
