﻿using System;

namespace mTSP.Solver
{
	class Printer
    {
		public static void Error(object msg)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine($"ERROR: {msg}");
			Console.ForegroundColor = ConsoleColor.White;
		}

		public static void Log(object msg)
		{
			Console.WriteLine(msg);
		}

		public static void Success(object msg)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine($"OK: {msg}");
			Console.ForegroundColor = ConsoleColor.White;
		}
    }
}
